import React from "react"
import styled from 'styled-components'

const AboutStyles = styled.div`
  padding: 1rem;
  margin: 3rem auto;
  max-width: 980px;
`

export default ({ data }) => (
  <AboutStyles>
    <h1>
      About {data.site.siteMetadata.title}
    </h1>
    <p>
      We're the only site running on your computer dedicated to showing the best
      photos and videos of pandas eating lots of food.
    </p>
  </AboutStyles>
)

export const query = graphql`
  query AboutQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
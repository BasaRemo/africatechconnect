import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

import logoImg from '../images/next.png'
import Feeds from '../Components/Feeds.js'
import Hero from '../Components/Hero.js'
import MapChart from '../Components/MapChart.js'
import SocialFeed from '../Components/SocialFeed.js'

const HomeStyles = styled.div`
  .home-content {
    padding: 1rem;
    margin: 3rem auto;
    max-width: 980px;
  }
`

const IndexPage = ( { data }) => (
  <HomeStyles>
  	<Hero></Hero>
  	<div className="home-content">
	    <Feeds data={data}></Feeds>
	    <h1 className="uk-heading">What's Coming in Africa?</h1>
	    <SocialFeed></SocialFeed>
  	</div>
  </HomeStyles>
)

export default IndexPage

export const query = graphql`
  query FeedsQuery {
    allContentfulEvent {
      edges {
        node {
          title
          description {
            description
          }
          whyAttend {
            whyAttend
          }
          startDate(formatString: "MMMM DD, YYYY")
          countryIso
          address
          url
          id
        }
      }
      totalCount
    }
  }
`

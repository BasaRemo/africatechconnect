import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'
import EventCard from '../Components/EventCard.js'
import UIkit from 'uikit';

import image from '../images/image.png'
import event2 from '../images/event2.png'
import event3 from '../images/event3.png'
import event4 from '../images/event3.png'
import event5 from '../images/event3.png'
import stlogo from '../images/stafricalogo.png'

const FeedsStyles = styled.div`

  .clean {
    background-color: #CF3060;
    color: white;
  }
`

export default ({ data }) => {

    return (

        <FeedsStyles className="content">
            
            <h1 className="uk-heading">Upcoming events : {data.allContentfulEvent.totalCount} </h1>

            <h3 className="uk-heading">January 2018</h3>

            <ul className="uk-grid-small uk-child-width-1-1@s uk-child-width-1-2@m uk-child-width-1-1@l uk-text-center" data-uk-grid="true">
                {data.allContentfulEvent.edges.map(({ node }) => (
                    <li key={node.id}>
                        <EventCard event={node} ></EventCard>
                    </li>
                ))}
            </ul>
        </FeedsStyles>
    );
        
};


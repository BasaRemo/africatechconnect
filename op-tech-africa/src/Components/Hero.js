import React from 'react'
import styled from 'styled-components'
//
import logoImg from '../images/background-next.png'

const HeroStyles = styled.div`
  .header-image {
    margin-top: -48px;
  }

  @media screen and (max-width:450px) {
       .header-image {
           background: url("http://placehold.it/300x300") no-repeat 0 0;
       }
  }

`

export default () => (
  <HeroStyles>
    <img className="header-image" src={logoImg} alt="" />
  </HeroStyles>
)

import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'
import ToggleDisplay from 'react-toggle-display';

import FlagIcon from './FlagIcon.js'

import icon from '../images/icon.png'
import kinshasa from '../images/kin.png'

const EventCardStyles = styled.div`

  padding: 10px;

  .clean {
    background-color: #CF3060;
    color: white;
  }

  .text {
    font-family: Arial;
    text-align: left;
    color: #3A3A3A; 
  }

  .title {
    font-family: Arial;
    font-size: 1.5rem;
    line-height: 1.2;
    font-weight: bold;
    color: #3A3A3A;
    margin-bottom: 10px;
  }

  p {
    margin-bottom: 10px;
    font-family: Arial;
    font-size:1.0rem;
    color: #3A3A3A;
  }

  .details {
    padding: 1rem;
  }

  .why {
    font-weight: 600;
    font-size: 1.1rem;
  }

  .flag-icon {
    float: right;
    padding-top: 20px;
  }
`

const LocationDivStyled = styled.div`
    color: black;
    font-weight: bold;

`

const DateDivStyled = styled.div`
    color: black; 
    font-weight: bold;
`

const BottomStyledDiv = styled.div`
    padding-top: 1rem;
`

export default ({ event }) => {
  return (
    <EventCardStyles className="uk-card uk-card-default uk-box-shadow-small">
      <div className="details">
          <div className="title text"><a style={{display: "table-cell"}} href={event.url} target="_blank">{event.title}</a></div>
          <p className="text"><span className="why">Description: </span> {event.description.description} </p>
          <p className="text"><span className="why">Why attend: </span> {event.whyAttend.whyAttend} </p>
          
          <BottomStyledDiv>
            <FlagIcon code={event.countryIso} size="2x" />
            <div className="uk-flex uk-flex-column text">
                <LocationDivStyled>{event.address}</LocationDivStyled>
                <DateDivStyled>{event.startDate}</DateDivStyled>
            </div>
          </BottomStyledDiv>
      </div>
    </EventCardStyles>
  );
};
import React from 'react'
import styled from 'styled-components'

import InstagramEmbed from 'react-instagram-embed'
import TweetEmbed from 'react-tweet-embed'

const SocialStyles = styled.div`

  .clean {
    background-color: #CF3060;
    color: white;
  }

  h4 {
    margin-bottom:5px;
    font-weight: 500;
  }

  h1 {
    margin-bottom:2.0rem;
    font-weight: 500;
  }
`
export default () => (
  <SocialStyles>
    <h1 className="uk-heading">What's Happening in Africa?</h1>
    <ul className="uk-grid-medium uk-child-width-1-3@s uk-text-center" data-uk-grid-parallax="true">
        <li>
            <h4 className="uk-heading uk-text-left">Augment Africa!</h4>
            <div className="uk-card uk-card-default uk-box-shadow-small">
            <InstagramEmbed url='https://instagr.am/p/BajN8d9g3WG/' hideCaption={true} containerTagName='div'/>
            </div>
        </li>
        <li>
            <h4 className="uk-heading uk-text-left">Start-up Weekend</h4>
            <div className="uk-card uk-card-default uk-box-shadow-small">
            <InstagramEmbed url='https://instagr.am/p/Bc747b1HgYt/' hideCaption={true} containerTagName='div'/>
            </div>
        </li>
        <li>
            <h4 className="uk-heading uk-text-left">Because ball is life loll</h4>
            <div className="uk-card uk-card-default uk-box-shadow-small">
            <InstagramEmbed url='https://instagr.am/p/BdCG76WlWdp/' hideCaption={true} containerTagName='div'/>
            </div>
        </li>
        <li>
            <h4 className="uk-heading uk-text-left">Don't miss you shot..Apply!</h4>
            <div className="uk-card uk-card-default uk-box-shadow-small">
            <InstagramEmbed url='https://instagr.am/p/BYBt87kFA24/' hideCaption={true} containerTagName='div'/>
            </div>
        </li>
        <li>
            <h4 className="uk-heading uk-text-left">Because ball is life loll</h4>
            <TweetEmbed id='692527862369357824' />
        </li>
        <li>
            <h4 className="uk-heading uk-text-left">Don't miss you shot..Apply!</h4>
            <TweetEmbed id='692527862369357824' />
        </li>
    </ul>
  </SocialStyles>
)

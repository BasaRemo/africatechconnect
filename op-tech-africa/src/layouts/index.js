import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'

import styled from 'styled-components'

import './index.css'
import 'uikit/dist/css/uikit.min.css';

import CustomNavBar from '../Components/CustomNavBar.js'

const AppStyles = styled.div`
  a {
    color: #126AD1;
    font-weight: bold;
  }

  img {
    max-width: 100%;
  }
  background-color:#F2F2F2;
`

const TemplateWrapper = ({ children, data }) => (
  <div>
    <Helmet
      title={data.site.siteMetadata.title}
      meta={[
        { name: 'description', content: 'Sample' },
        { name: 'keywords', content: 'sample, something' },
      ]}
      link={
        [
          {rel: 'shortcut icon', href: '../images/favicon.png'}
        ]
      }
    />
    <CustomNavBar title={data.site.siteMetadata.title}></CustomNavBar>
    <AppStyles>
      <div className="content">
        {children()}
      </div>
    </AppStyles>
  </div>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper

export const query = graphql`
  query LayoutQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
